function getInvoices(e) {
	e.preventDefault();

	let vatNumber = document.getElementById("vat").value;
	const formData = `vatNumber=${vatNumber}`;
	const request = new XMLHttpRequest();
	request.open("GET", "/cgi-bin/getInvoices?" + formData);
	request.send();
}
