# SWS02 Connect a family member to an External Service API

# 1. Requirements

_As a SWS family finance application prototype administrator, I want to connect a family member to an external service API (E-Fatura; Financial Services Provider; Public Utility)._

A Family Member is a user of this application, that was previously added by the family administrator.
To connect the family member with any of the services we will use VAT number, that is the unique identification number of each person that is common to all services.

```plantuml
@startuml
header SSD
title Connect a family member to an External Service API
autonumber
actor "Admininstrator" as Admin
participant ": Application" as App

Admin -> App : Connect a family member to an External Service API
activate Admin
activate App
return Give a list of family members to choose, \nlist of services and ask intendeed URI
deactivate App

Admin -> App : choose family member, API and inputs URI
activate App

return inform success

deactivate Admin
@enduml
```

# 2. Analysis

## 2.1 Family Member

A Family Member has the following attributes:

| Attributes | Rules                                     |
| ---------- | ----------------------------------------- |
| name       | required, alphabetic, with spaces, String |
| VAT Number | required, numeric, 9 digits               |

## 2.2 API

There are only three types of API:

- E-Fatura;
- Finantial Services;
- Public Utilities.

## 2.2 API URI

The administrator needs to input the API URI that will follow the following structure: `http://<link-to-api.com>/?u=$memberVat`.

# 3. Design

## 3.1. Functionality Development

Regarding the connection of the external API's to a specific family member, we should accommodate the requirements
specified in [Analysis](#2-analysis).

The System Diagram is the following:

```plantuml
@startuml
header SD
title Add a family member and its ledger
autonumber
actor "System\nAdministrator" as Admin
participant "Webpage" as UI
participant "connectServices.js" as JS
participant "connectServices.bash" as App

Admin -> UI : Connect a family member to External API
activate Admin
activate UI
return Give a list of family members to choose, \nlist of services and ask intendeed URI
deactivate UI

Admin -> UI : choose family member, API and inputs URI
activate UI
UI -> JS : connectFamilyMemberToExternalService\n(familyMember, ExternalService)
activate JS

JS -> JS : memberVat=document.getElementById\n("member").value
JS -> JS : service=document.getElementById\n("service").value
JS -> JS : uri=document.getElementById\n("uri").value

JS -> App : request(POST, memberVat)
activate App
ref over App
    connectServices.bash
end ref
return inform success
return inform success
return inform success

deactivate Admin
@enduml
```

```plantuml
@startuml
header ref
title connectServices.bash
autonumber
participant "connectServices.bash" as BASH


[-> BASH : request(POST, memberVat)
activate BASH

BASH -> BASH : read QUERY_STRING_POST

BASH -> BASH : VAT=$(echo ${QUERY_STRING_POST} | jq '.member')

BASH -> BASH : SERVICE=$(echo ${QUERY_STRING_POST} | jq '.service' | tr -d \")

BASH -> BASH : URI=$(echo ${QUERY_STRING_POST} | jq '.uri' | tr -d \")

BASH --> dir as "service.txt" ** : create file service.txt
[<-- BASH: boolean
deactivate BASH
@enduml
```

When the System Administrator chooses the intended member and API and inputs the URI, the application should then operate the required processes connecting an External Service to a family member.

# 4. Implementation

In order to fulfill this requirement the biggest challenge was to decide where to present this functionality. First we thought that we could have this functionality inside the html member page but afterwards we decided that would be better for the family administrator to have this option at the main page and then choose the intended member and service.

To de-structure JSON message, we opt to install `jq`, a parser for JSON objects.

In short, to get the value for a JSON key, we simply invoke `jq ".${key}"`.

# 5. Integration/Demonstration

This US depends on the [SWS01] because we already need to have at least one family members to connect any service. Depends on the provided API's of the external services too.

# 6. Comments

[sws01]: SWS01-Add-Family-Member.md
