# SWS01 Add a Family Member and its Ledger

# 1. Requirements

_As a SWS family finance application prototype administrator, I want to add a family member and its dedicated ledger._

A Family Member is a user of this application.
A Ledger is a a book or other collection of financial accounts.

A Ledger can be represented, e.g., as the following table:

| Date   | Description | Debit |  Credit | Balance |
| ------ | ----------- | ----: | ------: | ------: |
| 15-May | Salary      |       | 1896.34 | 1896.34 |
| 16-May | Groceries   | 98.54 |         | 1797.80 |
|        |             |       |         |         |

```plantuml
@startuml
header SSD
title Add a family member
autonumber
actor "Administrator" as Admin
participant ": Application" as App

Admin -> App : Add a family member and its ledger
activate Admin
activate App
return ask family member name
deactivate App

Admin -> App : input family member name
activate App

return inform success

deactivate Admin
@enduml
```

# 2. Analysis

## 2.1 Family Member

A Family Member should have the following attributes:

| Attributes | Rules                                     |
| ---------- | ----------------------------------------- |
| name       | required, alphabetic, with spaces, String |
| VAT Number | required, numeric, 9 digits               |

# 3. Design

## 3.1. Functionality Development

Regarding the creation of a new family member, we should accommodate the requirements
specified in [Analysis](#2-analysis).

The System Diagram is the following:

```plantuml
@startuml
header SD
title Add a family member and its ledger
autonumber
actor "System\nAdministrator" as Admin
participant "Webpage" as UI
participant "familyMembers.js" as JS
participant "familyMembers.bash" as App

Admin -> UI : Add a family member
activate Admin
activate UI
return ask family member name
deactivate UI

Admin -> UI : input family member name
activate UI
UI -> JS : createFamilyMember(new-family-member)
activate JS

JS -> JS : memberName=document.getElementById\n("new-member-name").value

JS -> App : request(POST, memberName)
activate App
ref over App
    familyMembers.bash
end ref
return inform success
return inform success
return inform success

deactivate Admin
@enduml
```

```plantuml
@startuml
header ref
title familyMembers.bash
autonumber
participant "familyMembers.bash" as BASH


[-> BASH : request(POST, memberName)
activate BASH

BASH -> BASH : read QUERY_STRING_POST

BASH -> BASH : MEMBER_NAME=cut $QUERY_STRING_POST

BASH --> dir as "$MEMBER_NAME" ** : mkdir $MEMBER_NAME
activate dir
return $MEMBER_NAME
[<-- BASH: boolean
deactivate BASH
@enduml
```

When the System Administrator inputs the required data for a family member to be created,
the application should then operate the required processes creating a new and valid
family member.

# 4. Implementation

# 5. Integration/Demonstration

# 6. Comments
