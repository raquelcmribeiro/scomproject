# E-fatura01 Add a Taxable Person Account

# 1. Requirements

As E-Fatura mock administrator, I want to add a taxable person account (VAT
identification number).

- Name
- VAT number


```plantuml
@startuml
header SSD
title Add a taxable person account
autonumber
actor "Admininstrator" as Admin
participant ": Application" as App

Admin -> App : Add a taxable person account
activate Admin
activate App
return ask data
deactivate App

Admin -> App : input data
activate App

return inform success

deactivate Admin
@enduml
```

# 2. Analysis

## 2.1 Person Account

As mentioned above, a taxable person account must have the following attributes:

| Attributes | Rules                                     |
| ---------- | ----------------------------------------- |
| name       | required, alphabetic, with spaces, String |
| VAT Number | required, numeric, 9 digits               |

# 3. Design

## 3.1. Functionality Development

The System Diagram is the following:

```plantuml
@startuml
header SD
title Add a taxable person account
autonumber
actor "System\nAdmininstrator" as Admin
participant "Index Webpage" as UI
participant "Add Account Webpage" as AA
participant "personAccount.js" as JS
participant "request \n: XMLHttpRequest" as req
participant "personAccount.bash" as App

Admin -> UI : Add a taxable person account
activate Admin
activate UI
UI -> AA : Add taxable person account
deactivate UI
activate AA
AA -> Admin: ask person name \nand VAT number
deactivate AA

Admin -> AA : input person name and VAT number
activate AA
AA -> JS : addPersonAccount()
activate JS

JS -> JS : memberName=document.getElementById\n("name").value
JS -> JS : memberVAT=document.getElementById\n("VAT").value

JS --> req* : create()

req -> App: send(data)
activate req
deactivate req
activate App
ref over App
    send(data)
end ref
App --> JS : informa success
deactivate App
return inform success
return inform success

deactivate Admin
@enduml
```

```plantuml
@startuml
header ref
title familyMembers.bash
autonumber
participant "familyMembers.bash" as BASH


[-> BASH : request(POST, data)
activate BASH

BASH -> BASH : read QUERY_STRING_POST

BASH -> BASH : MEMBER_NAME=cut $QUERY_STRING_POST
BASH -> BASH : MEMBER_VAT=cut $QUERY_STRING_POST


BASH --> dir as "$MEMBER_NAME" ** : mkdir $MEMBER_VAT
activate dir
return $MEMBER_NAME
[<-- BASH: boolean
deactivate BASH
@enduml
```

Validação da informação

# 4. Implementation

# 5. Integration/Demonstration

# 6. Comments
